class Statement{
	private id:number;
	private sentence:string;

	constructor(id:number, sentence:string){
		this.id = id;
		this.sentence = sentence;
	}	
	
	public getId(){
		return this.id;
	}
    
    public getText(){
        return this.sentence;    
    }
	
}

class TreeNode{
	private id:number;
	private sentence:Statement;

	constructor(id:number, sentence:Statement){
		this.id = id;
		this.sentence = sentence;
	}	
	
	public getId(){
		return this.id;
	}
	
	public getText(){
		return this.sentence.getText();
	}
}

class DecisionNode extends TreeNode{

	private yesNode :TreeNode;
	private noNode :TreeNode;

	constructor(id:number, sentence:Statement){
		super(id, sentence);
	}
    
    public setYesNode(node:TreeNode){
        this.yesNode=node;    
    }
    
    public setNoNode(node:TreeNode){
        this.noNode=node;    
    }
    
    public getYesNode(){
        return this.yesNode;
    }
    
    public getNoNode(){
        return this.noNode;    
    }
}

class ActionNode extends TreeNode{
    
    constructor(id:number, sentence:Statement){
        super(id, sentence);
    }
}

class TreeNodeFactory{
    
    private statements :Statement[] = new Array(17);
    private treeNodes :TreeNode[] = new Array(20);
    
    //constructor(conteneur:HTMLElement){
    constructor(){    
       // this.conteneur = conteneur;
       // this.createConteneur();
        this.initStatements();        
        this.initTreeNodes();
    }
    
    public createConteneur(){
       this.createConteneurBefore(null);   
    }
     
    public createConteneurBefore(nextSiblingLocation:HTMLElement){
                      
        var treeNodeConteneur = document.createElement("div");
        treeNodeConteneur.setAttribute("id", "decisionTree");
        
        var phraseConteneur = document.createElement("span");
        phraseConteneur.setAttribute("id", "phrase");
        
        var btnYes = document.createElement("button");
        btnYes.setAttribute("id","btnYes");
        btnYes.textContent = "Oui";
        
        var btnNo = document.createElement("button");
        btnNo.setAttribute("id","btnNo"); 
        btnNo.textContent = "Non";      
        
        treeNodeConteneur.appendChild(phraseConteneur);
        treeNodeConteneur.appendChild(btnYes);
        treeNodeConteneur.appendChild(btnNo);
        
        if(nextSiblingLocation == null){
            
            console.log("je suis null");
        
            document.body.insertBefore(treeNodeConteneur, document.body.lastChild);
        }
        else{
        
            nextSiblingLocation.parentElement.insertBefore(treeNodeConteneur, nextSiblingLocation);
        }
               
    }
    
    public getTreeNodeById(id:number){
        
       if(id<0 || id>this.treeNodes.length){
            throw new RangeError("L'indice "+id+" n'existe pas. Impossible de retrouver un objet TreeNode !");    
        }
        
        return this.treeNodes[id];
    
    }
    
    public getStatementById(id:number){
        
        if(id<0 || id>this.statements.length){
            throw new RangeError("L'indice "+id+" n'existe pas. Impossible de retrouver un objet Statement !");    
        }
        
        return this.statements[id];        
        
    }
    
    private initTreeNodes(){
        
                //"La documentation est disponible ici."
        var action1 = new ActionNode(0, this.getStatementById(11));
        
        //"La réponse est 42... allez recommence, un moment donné il faut apprendre à dire oui..."
        var action2 = new ActionNode(1, this.getStatementById(10));
        
        //"Lire le readme.md"
        var action3 = new ActionNode(2, this.getStatementById(9));
        
        //"Lire la doc pourrait aider à mieux comprendre."
        var action4 = new ActionNode(3, this.getStatementById(8));
        
        //"Le support technique est disponible via une demande clearquest."
        var action5 = new ActionNode(4, this.getStatementById(12));
        
        //"Le support technique pour former est disponible via une demande clearquest."
        var action6 = new ActionNode(5, this.getStatementById(13));
        
        //"Pour contribuer aux évolutions voici la démarche à suivre."
        var action7 = new ActionNode(6, this.getStatementById(14));
        
        //"Pour contribuer aux MCO voici la démarche à suivre."
        var action8 = new ActionNode(7, this.getStatementById(15));
        
        //Parcourir PHPK_D et le wiki.
        var action9 = new ActionNode(8, this.getStatementById(16));
        
        //Je veux un support technique ?
        var decision1 = new DecisionNode(9, this.getStatementById(6));
        decision1.setYesNode(action5);
        decision1.setNoNode(action2);
        
        //Je veux une formation ?
        var decision2 = new DecisionNode(10, this.getStatementById(5));
        decision2.setYesNode(action6);
        decision2.setNoNode(decision1);
        
        //Je veux contacter l'équipe
        var decision3 = new DecisionNode(11, this.getStatementById(4));
        decision3.setYesNode(decision2);
        decision3.setNoNode(action2);
        
        //Je veux contribuer aux évolutions
        var decision4 = new DecisionNode(12, this.getStatementById(3));
        decision4.setYesNode(action7);
        decision4.setNoNode(decision3);
        
        //Je veux contribuer aux MCO
        var decision5 = new DecisionNode(13, this.getStatementById(2));
        decision5.setYesNode(action8);
        decision5.setNoNode(decision4);
        
        //Je veux bien lire la doc ?"
        var decision6 = new DecisionNode(14, this.getStatementById(7));
        decision6.setYesNode(action9);
        decision6.setNoNode(decision2);
                
        //Je connais PHPJ ?"
        var decision7 = new DecisionNode(15, this.getStatementById(1));
        decision7.setYesNode(action3);
        decision7.setNoNode(decision6);
        
        //Je veux créer un projet avec PHPJ ?"
        var decision8 = new DecisionNode(16, this.getStatementById(0));
        decision8.setYesNode(decision7);
        decision8.setNoNode(decision5);
        
        this.treeNodes.length=0;
        
        this.treeNodes.push(action1);
        this.treeNodes.push(action2);
        this.treeNodes.push(action3);
        this.treeNodes.push(action4);
        this.treeNodes.push(action5);
        this.treeNodes.push(action6);
        this.treeNodes.push(action7);
        this.treeNodes.push(action8);
        this.treeNodes.push(action9);
        this.treeNodes.push(decision1);
        this.treeNodes.push(decision2);
        this.treeNodes.push(decision3);
        this.treeNodes.push(decision4);
        this.treeNodes.push(decision5);
        this.treeNodes.push(decision6);
        this.treeNodes.push(decision7);
        this.treeNodes.push(decision8);
    
    
    }
    
    private initStatements(){ 
        
        this.statements.length=0;
        this.statements.push(new Statement(0, "Je veux créer un projet avec PHPJ ?"));
        this.statements.push(new Statement(1, "Je connais PHPJ ?"));
        this.statements.push(new Statement(2, "Je veux contribuer au MCO ?"));
        this.statements.push(new Statement(3, "Je veux contribuer aux évolutions ?"));
        this.statements.push(new Statement(4, "Je veux contacter l'équipe ?"));
        this.statements.push(new Statement(5, "Je veux une formation ?"));
        this.statements.push(new Statement(6, "Je veux un support technique ?"));
        this.statements.push(new Statement(7, "Je veux bien lire la doc ?"));
        this.statements.push(new Statement(8, "Lire la doc pourrait aider à mieux comprendre."));
        this.statements.push(new Statement(9, "Lire le readme.md"));
        this.statements.push(new Statement(10, "La réponse est 42... allez recommence, un moment donné il faut apprendre à dire oui..."));
        this.statements.push(new Statement(11, "La documentation est disponible ici."));
        this.statements.push(new Statement(12, "Le support technique est disponible via une demande clearquest."));
        this.statements.push(new Statement(13, "Le support technique pour former est disponible via une demande clearquest."));
        this.statements.push(new Statement(14, "Pour contribuer aux évolutions voici la démarche à suivre:."));
        this.statements.push(new Statement(15, "Pour contribuer aux MCO voici la démarche à suivre:."));
        this.statements.push(new Statement(16, "Parcourir PHPK_D et le wiki."));

        
    }

}


/*
*IHM
*/
document.addEventListener("DOMContentLoaded", function(event) {

console.log("DOM fully loaded and parsed");
    
var decisionTree = new TreeNodeFactory(); 
    
   // decisionTree.createConteneurBefore(document.getElementById("voisin-bas"));
    
    decisionTree.createConteneur();

let buttonYes = <HTMLElement>document.body.querySelector("#btnYes");
buttonYes.addEventListener("click", (e) =>{
    console.log(e.target["value"]); 
    
    var node = decisionTree.getTreeNodeById(e.target["value"]);
    
    displayNode(node);
   
});
    
let buttonNo = <HTMLElement>document.body.querySelector("#btnNo");
buttonNo.addEventListener("click", (e) =>{
    console.log(e.target["value"]);  
    
     var node = decisionTree.getTreeNodeById(e.target["value"]);
    
    displayNode(node);
    
});
    
    displayNode(decisionTree.getTreeNodeById(16));
    
});

function displayNode(node){
    
    if(node instanceof DecisionNode){
        var decision = <DecisionNode> node;
        updateDecisionTree(decision.getText(), decision.getYesNode().getId(), decision.getNoNode().getId());
    }
    else{
        var action = <ActionNode> node;
        updateDecisionTree(action.getText(), 16, null);    
    }
}

function updateDecisionTree(phrase, valueBtn1, valueBtn2){  
   
    var statement = document.getElementById("phrase");
    
    statement.innerText = phrase; 
    
    if(valueBtn2 == null){
        updateAction(valueBtn1);
    }
    else{
        updateDecision(valueBtn1, valueBtn2);
    }

}

function updateDecision(valueBtn1, valueBtn2){
    
    var btnYes = document.getElementById('btnYes');    
   
    btnYes.setAttribute("value", valueBtn1);
    
    btnYes.textContent = "Oui";
    
    var btnNo = document.getElementById('btnNo');   
    
    btnNo.setAttribute("style", "display: inline");
   
    btnNo.setAttribute("value", valueBtn2);

}

function updateAction(valueBtn1){
    
    var btnYes = document.getElementById('btnYes');  
    
    btnYes.setAttribute("value", valueBtn1);
    
    btnYes.textContent = "Click moi pour recommencer le questionnaire";
    
    var btnNo = document.getElementById('btnNo');  
    
    btnNo.setAttribute("style", "display: none");
}

    





//class Decision