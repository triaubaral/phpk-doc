var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Statement = (function () {
    function Statement(id, sentence) {
        this.id = id;
        this.sentence = sentence;
    }
    Statement.prototype.getId = function () {
        return this.id;
    };
    Statement.prototype.getText = function () {
        return this.sentence;
    };
    return Statement;
}());
var TreeNode = (function () {
    function TreeNode(id, sentence) {
        this.id = id;
        this.sentence = sentence;
    }
    TreeNode.prototype.getId = function () {
        return this.id;
    };
    TreeNode.prototype.getText = function () {
        return this.sentence.getText();
    };
    return TreeNode;
}());
var DecisionNode = (function (_super) {
    __extends(DecisionNode, _super);
    function DecisionNode(id, sentence) {
        _super.call(this, id, sentence);
    }
    DecisionNode.prototype.setYesNode = function (node) {
        this.yesNode = node;
    };
    DecisionNode.prototype.setNoNode = function (node) {
        this.noNode = node;
    };
    DecisionNode.prototype.getYesNode = function () {
        return this.yesNode;
    };
    DecisionNode.prototype.getNoNode = function () {
        return this.noNode;
    };
    return DecisionNode;
}(TreeNode));
var ActionNode = (function (_super) {
    __extends(ActionNode, _super);
    function ActionNode(id, sentence) {
        _super.call(this, id, sentence);
    }
    return ActionNode;
}(TreeNode));
var TreeNodeFactory = (function () {
    //constructor(conteneur:HTMLElement){
    function TreeNodeFactory() {
        this.statements = new Array(17);
        this.treeNodes = new Array(20);
        // this.conteneur = conteneur;
        // this.createConteneur();
        this.initStatements();
        this.initTreeNodes();
    }
    TreeNodeFactory.prototype.createConteneur = function () {
        this.createConteneurBefore(null);
    };
    TreeNodeFactory.prototype.createConteneurBefore = function (nextSiblingLocation) {
        var treeNodeConteneur = document.createElement("div");
        treeNodeConteneur.setAttribute("id", "decisionTree");
        var phraseConteneur = document.createElement("span");
        phraseConteneur.setAttribute("id", "phrase");
        var btnYes = document.createElement("button");
        btnYes.setAttribute("id", "btnYes");
        btnYes.textContent = "Oui";
        var btnNo = document.createElement("button");
        btnNo.setAttribute("id", "btnNo");
        btnNo.textContent = "Non";
        treeNodeConteneur.appendChild(phraseConteneur);
        treeNodeConteneur.appendChild(btnYes);
        treeNodeConteneur.appendChild(btnNo);
        if (nextSiblingLocation == null) {
            console.log("je suis null");
            document.body.insertBefore(treeNodeConteneur, document.body.lastChild);
        }
        else {
            nextSiblingLocation.parentElement.insertBefore(treeNodeConteneur, nextSiblingLocation);
        }
    };
    TreeNodeFactory.prototype.getTreeNodeById = function (id) {
        if (id < 0 || id > this.treeNodes.length) {
            throw new RangeError("L'indice " + id + " n'existe pas. Impossible de retrouver un objet TreeNode !");
        }
        return this.treeNodes[id];
    };
    TreeNodeFactory.prototype.getStatementById = function (id) {
        if (id < 0 || id > this.statements.length) {
            throw new RangeError("L'indice " + id + " n'existe pas. Impossible de retrouver un objet Statement !");
        }
        return this.statements[id];
    };
    TreeNodeFactory.prototype.initTreeNodes = function () {
        //"La documentation est disponible ici."
        var action1 = new ActionNode(0, this.getStatementById(11));
        //"La réponse est 42... allez recommence, un moment donné il faut apprendre à dire oui..."
        var action2 = new ActionNode(1, this.getStatementById(10));
        //"Lire le readme.md"
        var action3 = new ActionNode(2, this.getStatementById(9));
        //"Lire la doc pourrait aider à mieux comprendre."
        var action4 = new ActionNode(3, this.getStatementById(8));
        //"Le support technique est disponible via une demande clearquest."
        var action5 = new ActionNode(4, this.getStatementById(12));
        //"Le support technique pour former est disponible via une demande clearquest."
        var action6 = new ActionNode(5, this.getStatementById(13));
        //"Pour contribuer aux évolutions voici la démarche à suivre."
        var action7 = new ActionNode(6, this.getStatementById(14));
        //"Pour contribuer aux MCO voici la démarche à suivre."
        var action8 = new ActionNode(7, this.getStatementById(15));
        //Parcourir PHPK_D et le wiki.
        var action9 = new ActionNode(8, this.getStatementById(16));
        //Je veux un support technique ?
        var decision1 = new DecisionNode(9, this.getStatementById(6));
        decision1.setYesNode(action5);
        decision1.setNoNode(action2);
        //Je veux une formation ?
        var decision2 = new DecisionNode(10, this.getStatementById(5));
        decision2.setYesNode(action6);
        decision2.setNoNode(decision1);
        //Je veux contacter l'équipe
        var decision3 = new DecisionNode(11, this.getStatementById(4));
        decision3.setYesNode(decision2);
        decision3.setNoNode(action2);
        //Je veux contribuer aux évolutions
        var decision4 = new DecisionNode(12, this.getStatementById(3));
        decision4.setYesNode(action7);
        decision4.setNoNode(decision3);
        //Je veux contribuer aux MCO
        var decision5 = new DecisionNode(13, this.getStatementById(2));
        decision5.setYesNode(action8);
        decision5.setNoNode(decision4);
        //Je veux bien lire la doc ?"
        var decision6 = new DecisionNode(14, this.getStatementById(7));
        decision6.setYesNode(action9);
        decision6.setNoNode(decision2);
        //Je connais PHPJ ?"
        var decision7 = new DecisionNode(15, this.getStatementById(1));
        decision7.setYesNode(action3);
        decision7.setNoNode(decision6);
        //Je veux créer un projet avec PHPJ ?"
        var decision8 = new DecisionNode(16, this.getStatementById(0));
        decision8.setYesNode(decision7);
        decision8.setNoNode(decision5);
        this.treeNodes.length = 0;
        this.treeNodes.push(action1);
        this.treeNodes.push(action2);
        this.treeNodes.push(action3);
        this.treeNodes.push(action4);
        this.treeNodes.push(action5);
        this.treeNodes.push(action6);
        this.treeNodes.push(action7);
        this.treeNodes.push(action8);
        this.treeNodes.push(action9);
        this.treeNodes.push(decision1);
        this.treeNodes.push(decision2);
        this.treeNodes.push(decision3);
        this.treeNodes.push(decision4);
        this.treeNodes.push(decision5);
        this.treeNodes.push(decision6);
        this.treeNodes.push(decision7);
        this.treeNodes.push(decision8);
    };
    TreeNodeFactory.prototype.initStatements = function () {
        this.statements.length = 0;
        this.statements.push(new Statement(0, "Je veux créer un projet avec PHPJ ?"));
        this.statements.push(new Statement(1, "Je connais PHPJ ?"));
        this.statements.push(new Statement(2, "Je veux contribuer au MCO ?"));
        this.statements.push(new Statement(3, "Je veux contribuer aux évolutions ?"));
        this.statements.push(new Statement(4, "Je veux contacter l'équipe ?"));
        this.statements.push(new Statement(5, "Je veux une formation ?"));
        this.statements.push(new Statement(6, "Je veux un support technique ?"));
        this.statements.push(new Statement(7, "Je veux bien lire la doc ?"));
        this.statements.push(new Statement(8, "Lire la doc pourrait aider à mieux comprendre."));
        this.statements.push(new Statement(9, "Lire le readme.md"));
        this.statements.push(new Statement(10, "La réponse est 42... allez recommence, un moment donné il faut apprendre à dire oui..."));
        this.statements.push(new Statement(11, "La documentation est disponible ici."));
        this.statements.push(new Statement(12, "Le support technique est disponible via une demande clearquest."));
        this.statements.push(new Statement(13, "Le support technique pour former est disponible via une demande clearquest."));
        this.statements.push(new Statement(14, "Pour contribuer aux évolutions voici la démarche à suivre:."));
        this.statements.push(new Statement(15, "Pour contribuer aux MCO voici la démarche à suivre:."));
        this.statements.push(new Statement(16, "Parcourir PHPK_D et le wiki."));
    };
    return TreeNodeFactory;
}());
/*
*IHM
*/
document.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
    var decisionTree = new TreeNodeFactory();
    // decisionTree.createConteneurBefore(document.getElementById("voisin-bas"));
    decisionTree.createConteneur();
    var buttonYes = document.body.querySelector("#btnYes");
    buttonYes.addEventListener("click", function (e) {
        console.log(e.target["value"]);
        var node = decisionTree.getTreeNodeById(e.target["value"]);
        displayNode(node);
    });
    var buttonNo = document.body.querySelector("#btnNo");
    buttonNo.addEventListener("click", function (e) {
        console.log(e.target["value"]);
        var node = decisionTree.getTreeNodeById(e.target["value"]);
        displayNode(node);
    });
    displayNode(decisionTree.getTreeNodeById(16));
});
function displayNode(node) {
    if (node instanceof DecisionNode) {
        var decision = node;
        updateDecisionTree(decision.getText(), decision.getYesNode().getId(), decision.getNoNode().getId());
    }
    else {
        var action = node;
        updateDecisionTree(action.getText(), 16, null);
    }
}
function updateDecisionTree(phrase, valueBtn1, valueBtn2) {
    var statement = document.getElementById("phrase");
    statement.innerText = phrase;
    if (valueBtn2 == null) {
        updateAction(valueBtn1);
    }
    else {
        updateDecision(valueBtn1, valueBtn2);
    }
}
function updateDecision(valueBtn1, valueBtn2) {
    var btnYes = document.getElementById('btnYes');
    btnYes.setAttribute("value", valueBtn1);
    btnYes.textContent = "Oui";
    var btnNo = document.getElementById('btnNo');
    btnNo.setAttribute("style", "display: inline");
    btnNo.setAttribute("value", valueBtn2);
}
function updateAction(valueBtn1) {
    var btnYes = document.getElementById('btnYes');
    btnYes.setAttribute("value", valueBtn1);
    btnYes.textContent = "Click moi pour recommencer le questionnaire";
    var btnNo = document.getElementById('btnNo');
    btnNo.setAttribute("style", "display: none");
}
//class Decision 
